#!/usr/bin/env python

import csv
from os import getenv
from pgvector.sqlalchemy import Vector
from sqlalchemy import create_engine, insert, select, text, Integer, String, Text
from sqlalchemy.orm import declarative_base, mapped_column, Session
from sqlalchemy.dialects.postgresql import ARRAY
from alive_progress import alive_bar

PG_USER = getenv("PGUSER", "postgres")
PG_PASSWORD = getenv("PGPASSWORD", "vectorVICTOR")
PG_HOST = getenv("PGHOST", "localhost")
PG_PORT = getenv("PGPORT", "5432")
PG_DB = getenv("PGDB", "fashionvector")

engine = create_engine('postgresql+psycopg://{}:{}@{}:{}/{}'.format(PG_USER, PG_PASSWORD, PG_HOST, PG_PORT, PG_DB))
with engine.connect() as conn:
    conn.execute(text("CREATE EXTENSION IF NOT EXISTS vector"))
    conn.execute(text("CREATE EXTENSION IF NOT EXISTS google_ml_integration"))
    conn.commit()

Base = declarative_base()


class Catalog(Base):
    __tablename__ = "catalog"

    productid = mapped_column(Integer, primary_key=True)
    productname = mapped_column(Text)
    productbrand = mapped_column(String(36))
    gender = mapped_column(String(36))
    price = mapped_column(Integer)
    description = mapped_column(Text)
    primarycolor = mapped_column(String(36))
    embedding = mapped_column(Vector(768))


Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)
session = Session(engine)

with open('myntra_products_catalog.csv', 'r') as file:
    csv_reader = csv.DictReader(file)
    data = [ row for row in csv_reader ]

#print(data)

with engine.connect() as conn:
    for row in data:
        try:
            query = text("SELECT embedding( 'textembedding-gecko@001', '{}')".format(row['Description'].replace("'","").replace('?%', "")))
            e = conn.execute(query)
            for r in e:
                embed = r[0]
            conn.execute(
                    insert(Catalog), [dict(
                        productid=row['ProductID'],
                        productname=row['ProductName'],
                        productbrand=row['ProductBrand'],
                        gender=row['Gender'],
                        price=row['Price (INR)'],
                        primarycolor=row['PrimaryColor'],
                        description=row['Description'].replace("'","").replace('?%', ""),
                        embedding=embed,
                        )])
            conn.commit()
        except:
            print("Error with product {}".format(row['ProductID']))
