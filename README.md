# Fashion Vector

Show how to do hybrid queries

## Setup 

### Create the database

```
psql postgresql://USER:PASSWORD@IP:PORT/postgres
CREATE DATABASE fashionvector;
```

### Download the dataset 

`https://www.kaggle.com/datasets/shivamb/fashion-clothing-products-catalog?resource=download`

### Set the necessary environment variables

```
export PGUSER=postgres
export PGPASSWORD=<MYPASSWORD>
export PGHOST=<IP_ADDRESS>
export PGPORT=5432
export PGDB=fashionvector
```


### Load the dataset 

```
python3 -m venv .venv
source .venbin/bin/activate
pip install -r requirements.txt
./load_data.py
```

### Sample queries

```
select productname, price from catalog 
  ORDER BY 
  embedding::vector <-> embedding('textembedding-gecko@001', 'dark shirt with pockets')::vector
  LIMIT 30;
```

Slowly work to here

```
select productname, productbrand, gender, price from catalog
  WHERE price < 800 AND gender='Men' AND productbrand != 'Parx'
  ORDER BY
  embedding::vector <-> embedding('textembedding-gecko@001', 'dark shirt with pockets')::vector
  LIMIT 30;
```
